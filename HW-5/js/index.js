//1
function counter() {
    let a = 0
    return function(value) {
        a += value
        console.log(a)
    }
}
let b = counter()
b(3)
b(5)
b(7)

//2
function getUpdatedArr() {
    let arr = []
    return function(value) {
        if (value === undefined){
            arr = []
        }else {
            arr.push(value)
        }
        console.log(arr)
    }
}
let saveValueToArr = getUpdatedArr()
saveValueToArr(3)
saveValueToArr(5)
saveValueToArr({name: 'Vasya'})
saveValueToArr(4)
saveValueToArr()

//3
function getTime() {
    let  firstCall = false
    let a = new Date().getTime()
    return function() {
        if (firstCall) {
            let result = new Date().getTime() - a
            a = new Date().getTime()
            return  Math.floor(result / 1000)
        }else {
            firstCall = true
            return 'Enabled'
        }
    }
}
const counter = getTime()

//4
const timer = (secondsValue) => {
    let timeSeconds = 0
    let end = false
    const stopId = setInterval(() => {
        if (end){
            clearInterval(stopId)
            return console.log("Time End")
        }
        if(timeSeconds === 0 && !end){
            timeSeconds = secondsValue
        }
        let minutes = 0
        let seconds = 0
        if (timeSeconds / 60 > 0){
            minutes = Math.floor(timeSeconds / 60)
            seconds = timeSeconds - (minutes * 60)
        }
        console.log(minutes + ':' + seconds)
        --timeSeconds
        if(timeSeconds === 0){
            end = true
        }
    }, 1000, timeSeconds, end,secondsValue)
}

timer(60)