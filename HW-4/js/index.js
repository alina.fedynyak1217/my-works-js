const studentArr = [
    {
        name: 'Сергей',
        surname: 'Войлов',
        ratingPoint: 1000,
        schoolPoint: 1100,
        course: 2,
    },
    {
        name: 'Татьяна',
        surname: 'Коваленко',
        ratingPoint: 880,
        schoolPoint: 700,
        course: 1,
    },
    {
        name: 'Анна',
        surname: 'Кугир',
        ratingPoint: 730,
        schoolPoint: 1200,
        course: 3,
    },
    {
        name: 'Станислав',
        surname: 'Щелоков',
        ratingPoint: 1130,
        schoolPoint: 1060,
        course: 2,
    },
    {
        name: 'Денис',
        surname: 'Хрущ',
        ratingPoint: 1000,
        schoolPoint: 990,
        course: 4,
    },
    {
        name: 'Татьяна',
        surname: 'Капустник',
        ratingPoint: 650,
        schoolPoint: 500,
        course: 3,
    },
    {
        name: 'Максим',
        surname: 'Меженский',
        ratingPoint: 990,
        schoolPoint: 1100,
        course: 1,
    },
    {
        name: 'Денис',
        surname: 'Марченко',
        ratingPoint: 570,
        schoolPoint: 1300,
        course: 4,
    },
    {
        name: 'Антон',
        surname: 'Завадский',
        ratingPoint: 1090,
        schoolPoint: 1010,
        course: 3
    },
    {
        name: 'Игорь',
        surname: 'Куштым',
        ratingPoint: 870,
        schoolPoint: 790,
        course: 1,
    },
    {
        name: 'Инна',
        surname: 'Скакунова',
        ratingPoint: 1560,
        schoolPoint: 200,
        course: 2,
    },
];

(function() {
    if ( typeof Object.prototype.uniqueStudentId == "undefined" ) {
        let id = 0;
        Object.prototype.uniqueStudentId = function() {
            if ( typeof this.id == "undefined" ) {
                this.id = ++id;
            }
            return this.id;
        };
    }
})();
const allFreePlace = 5
class Student {

    static listOfStudents = []
    static freeLearnStudents = []
    static freePlace = 0

    constructor(enrollee) {
        this.id = this.uniqueStudentId()
        this.name = enrollee.name
        this.surname = enrollee.surname
        this.ratingPoint = enrollee.ratingPoint
        this.schoolPoint = enrollee.schoolPoint
        this.isSelfPayment  = this.checkContract(this.ratingPoint, this.schoolPoint)
        Student.listOfStudents.push(this)
        if(!this.isSelfPayment){
            Student.freeLearnStudents.push(this)
        }
    }

    checkContract(ratingPoint, schoolPoint) {
        if(ratingPoint >= 800 && Student.freePlace < allFreePlace){
            ++Student.freePlace
            return false;
        }else if(ratingPoint >= 800 && Student.freePlace > allFreePlace){
            let excludeStudentId = this.id
            let len = Student.freeLearnStudents.length
            let lowRatingPoint = this.ratingPoint
            for (let i = 0; i < len; i++){
                if(lowRatingPoint === Student.freeLearnStudents[i].ratingPoint){
                    if(schoolPoint > Student.freeLearnStudents[i].schoolPoint){
                        lowRatingPoint = Student.freeLearnStudents[i].ratingPoint
                        excludeStudentId = Student.freeLearnStudents[i].id
                    }
                }else if(lowRatingPoint > Student.freeLearnStudents[i].ratingPoint){
                    lowRatingPoint = Student.freeLearnStudents[i].ratingPoint
                    excludeStudentId = Student.freeLearnStudents[i].id
                }
            }
            if(excludeStudentId === this.id){
                return true
            }
            this.removeFreeLearn(excludeStudentId)
            return false
        } else{
            return true;
        }
    }

    removeFreeLearn(studentId){
        let len = Student.listOfStudents.length
        for (let i = 0; i < len; i++){
            if(Student.listOfStudents[i].id === studentId){
                Student.listOfStudents[i].isSelfPayment = true;
            }
        }
    }
}
let newUser = new Student(studentArr[2])
let newUser2 = new Student(studentArr[3])
let newUser3 = new Student(studentArr[4])
let newUser4 = new Student(studentArr[8])
let newUser5 = new Student(studentArr[5])
let newUser6 = new Student(studentArr[1])
let newUser7 = new Student(studentArr[6])
let newUser8 = new Student(studentArr[7])
let newUser9 = new Student(studentArr[9])

console.log(Student.listOfStudents)


//2
class  CustomString {
    reverse(string) {
        return string.split('').reverse().join('')
    }
    ucFirst(string) {
        return string[0].toUpperCase() + string.slice(1)
    }
    ucWords(string) {
        let arrStr = string.split(' ')
        let newArrStr = [];
        for(let i = 0; i < arrStr.length; i++){
            newArrStr[i] = arrStr[i][0].toUpperCase() + arrStr[i].slice(1)
        }
        //console.log(arrStr)
        return newArrStr.join(' ')
    }
 }
const myString = new CustomString()
console.log(myString.reverse('qwerty'))
console.log(myString.ucFirst('qwerty'))
console.log(myString.ucWords('qwerty qwerty qwerty'))

//3
class Validator {
    checkIsEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase())
    }
    checkIsDomain(domain) {
        const re = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/;
        return re.test(String(domain))
    }
    checkIsDate(date) {
       let dataIs =  Date.parse(date)
        if (isNaN(dataIs)) {
            return false
        }else{
            return  true
        }
    }
    checkIsPhone(phone) {
        return phone.includes('+38')
    }
}
let validator = new Validator()
console.log(validator.checkIsEmail('vasya.pupkin@gmail.com'))
console.log(validator.checkIsDomain('google.com'))
console.log(validator.checkIsDate('30.11.2019'))
console.log(validator.checkIsPhone('+38 (066) 937-99-92'))
