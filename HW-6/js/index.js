import {condidateArr} from './candidates.js'
//1
const removeUser = (arr, index) => {
    arr.forEach((currentValue, i) => {
        if (currentValue.index === index) {
            arr.splice(i, 1)
        }
    })
    return arr
}
console.log(removeUser(condidateArr, 1))

//..........
const removeUser = (arr, index) => {
  let newUsersArr =  arr.map((currentValue, i) => {
      if (currentValue.index === index) {
          arr.splice(i, 1)
      }
  })
    return arr
}
console.log(removeUser(condidateArr, 1))

//2
const getAllKeys = (obj) => {
    let newArray = []
    for (let value in obj) {
        newArray.push(value)
    }
    return newArray
}
console.log(getAllKeys(condidateArr[1]))

//3
const getAllValues = (obj) => {
    let newArray = []
    for (let value in obj) {
        newArray.push(obj[value])
    }
    return newArray
}
console.log(getAllValues(condidateArr[1]))

//4
const insertIntoArr = (candidate, id) => {
    const index = condidateArr.findIndex(item => item._id === id)
    condidateArr.splice(index, 0, candidate)
    return condidateArr
}
console.log(insertIntoArr(condidateArr[32], '5e216bc9cab1bd9dbae25637'))

//5
class Condidate {
    constructor(person){
        this.person = person
    }
    state() {
        let address = this.person.address
        let stringData = address.split(',')
        return stringData[2]
    }
}
const condidate = new Condidate(condidateArr[2])
console.log(condidate.state())

//6
const getCompanyNames = (arr) => {
    let newCompanyArr = arr.map(condidate => condidate.company)
    //console.log(newCompanyArr)
    return [... new Set(newCompanyArr)]
}
console.log(getCompanyNames(condidateArr))

//7
const getUsersByYear = (arr, year) => {
    let registeredYearArr = arr.filter(item => +item.registered.split('-')[0] === year)
        .map(item => item._id)
        return  registeredYearArr
}
console.log(getUsersByYear(condidateArr, 2015))

//8
const getCondidatesByUnreadMsg = (arr, unreadMsgCount) => {
    let newCondidatesByUnreadMsg = arr.filter(condidate => condidate.greeting.indexOf(unreadMsgCount) !== -1)
    return newCondidatesByUnreadMsg
}
console.log(getCondidatesByUnreadMsg(condidateArr,5))

//..........
const getCondidatesByUnreadMsg = (arr, unreadMsgCount) => arr.filter(condidate => condidate.greeting.includes(unreadMsgCount))
console.log(getCondidatesByUnreadMsg(condidateArr,5))

//9
const getCondidatesByGender = (arr, gender) => arr.filter(condidate => condidate.gender === gender)
console.log(getCondidatesByGender(condidateArr, 'female'))

//10
const arrayExample = [1,2,3,4,5]
Object.defineProperty(Array.prototype, 'alina_join', {
    value: function (seporator = ',') {
        let result = ''
        for (let index in this) {
            if (index != this.length -1) {
                result += this[index]+seporator
                continue
            }
            result += this[index]
        }
        return result
    }
})
console.log(arrayExample.alina_join())

//..........
Object.defineProperty(Array.prototype, 'alina_reduce', {
    value: function(callback, accumulator) {
       let startindex = 0
       if (accumulator == undefined) {
           startindex = 1
           accumulator = this[0]
       }
       for (let i = startindex; i < this.length; i++) {
           accumulator = callback(accumulator, this[i], i, this)
       }
       return accumulator
    }
})


