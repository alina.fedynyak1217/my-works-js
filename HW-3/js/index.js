const emplyeeArr = [
    {
        id: 1,
        name: 'Денис',
        surname: 'Хрущ',
        salary: 1010,
        workExperience: 10, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 2,
        name: 'Сергей',
        surname: 'Войлов',
        salary: 1210,
        workExperience: 12, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 3,
        name: 'Татьяна',
        surname: 'Коваленко',
        salary: 480,
        workExperience: 3, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'female'
    },
    {
        id: 4,
        name: 'Анна',
        surname: 'Кугир',
        salary: 2430,
        workExperience: 20, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'female'
    },
    {
        id: 5,
        name: 'Татьяна',
        surname: 'Капустник',
        salary: 3150,
        workExperience: 30, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'female'
    },
    {
        id: 6,
        name: 'Станислав',
        surname: 'Щелоков',
        salary: 1730,
        workExperience: 15, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 7,
        name: 'Денис',
        surname: 'Марченко',
        salary: 5730,
        workExperience: 45, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'male'
    },
    {
        id: 8,
        name: 'Максим',
        surname: 'Меженский',
        salary: 4190,
        workExperience: 39, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 9,
        name: 'Антон',
        surname: 'Завадский',
        salary: 790,
        workExperience: 7, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 10,
        name: 'Инна',
        surname: 'Скакунова',
        salary: 5260,
        workExperience: 49, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'female'
    },
    {
        id: 11,
        name: 'Игорь',
        surname: 'Куштым',
        salary: 300,
        workExperience: 1, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
];

class Emplyee {
   constructor(emplyee) {
    this.id = emplyee.id
    this.name = emplyee.name
    this.surname = emplyee.surname
    this.salary = emplyee.salary
    this.workExperience = emplyee.workExperience
    this.isPrivileges = emplyee.isPrivileges
    this.gender = emplyee.gender

   }
    getFullName() {
    return this.name + ' ' + this.surname
    }
}
let emplyee1 = new Emplyee(emplyeeArr[2])
console.log(emplyee1.getFullName())

class Emplyee2 {
    constructor(emplyeeArr) {
        this.emplyeeArr = emplyeeArr

    }
    createEmployesFromArr () {
        return this.emplyeeArr
    }
}
let emplyee2 = new Emplyee2(emplyeeArr)
const emplyeeConstructArr = emplyee2.createEmployesFromArr ()
console.log(emplyeeConstructArr)

const getFullNamesFromArr = (arr) => {
    let len = arr.length
    let arrFullName = []
    for (let i = 0; i < len; i++) {
         arrFullName[i] = arr[i].name + ' ' + arr[i].surname
    }
    return arrFullName
}
console.log(getFullNamesFromArr(emplyeeConstructArr))

const getMiddleSalary = (arr) => {
    let allSalary = 0
    let len = arr.length
    for (let i = 0; i < len; i++) {
        allSalary += arr[i].salary
    }
    let middleSalary = allSalary / len
    return middleSalary
}
console.log(getMiddleSalary(emplyeeConstructArr))

const getRandomEmployee = (arr) => {
    let len = arr.length
    let randomNum = Math.floor(Math.random() * len)
    return arr[randomNum]
}

console.log(getRandomEmployee(emplyeeConstructArr))

class Emplyee3 {
    constructor(emplyee) {
        this.id = emplyee.id
        this.name = emplyee.name
        this.surname = emplyee.surname
        this.salary = emplyee.salary
        this.workExperience = emplyee.workExperience
        this.isPrivileges = emplyee.isPrivileges
        this.gender = emplyee.gender
    }
    get fullInfo() {
        return {
            id: this.id,
            name: this.name,
            surname: this.surname,
            salary: this.salary,
            workExperience: this.workExperience,
            isPrivileges: this.isPrivileges,
            gender: this.gender
        }
    }

    set fullInfo(value) {
        this.id = value.id !== undefined ? value.id : this.id
        this.name = value.name !== undefined ? value.name : this.name
        this.surname = value.surname !== undefined ? value.surname : this.surname
        this.salary =  value.salary !== undefined ? value.salary : this.salary
        this.workExperience = value.workExperience !== undefined ? value.workExperience : this.workExperience
        this.isPrivileges = value.isPrivileges !== undefined ? value.isPrivileges : this.isPrivileges
        this.gender = value.gender !== undefined ? value.gender : this.gender
    }
}
let emplyee3 = new Emplyee3(emplyeeArr[2])
emplyee3.fullInfo = {name: 'Вeffefася', salary: 23424, email: 'ex@mail.ua'}
console.log(emplyee3.fullInfo)